using System;
using System.Collections.Generic;

namespace GoTo.Models
{
    public partial class lines
    {
        public int ID { get; set; }
        public string company { get; set; }
        public DateTime? created_at { get; set; }
        public string days { get; set; }
        public string end_station { get; set; }
        public string name { get; set; }
        public string start_station { get; set; }
        public int? transporttype_id { get; set; }
        public DateTime? updated_at { get; set; }
    }
}
