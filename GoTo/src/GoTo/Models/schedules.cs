using System;
using System.Collections.Generic;

namespace GoTo.Models
{
    public partial class schedules
    {
        public int id { get; set; }
        public TimeSpan? arrival { get; set; }
        public DateTime? created_at { get; set; }
        public int? line_id { get; set; }
        public string name { get; set; }
        public int? station_id { get; set; }
        public int direction { get; set; } = 0;
        public DateTime? updated_at { get; set; }
        public List<trips> Trips { get; set; }
    }
}
