﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GoTo.Models
{
    public class GoToRepository: IGoToRepository
    {
        private GoToDbContext context;

        public GoToRepository(GoToDbContext context)
        {
            this.context = context;
        }

        public List<Route> GetRoute(string startingStation, string endStation, TimeSpan? selectedTime, string sortOption = "duration")
        {
            List<Route> directRoutes = new List<Route>();
            Dictionary<int, Route> indirectRoutes = new Dictionary<int, Route>();

            int startingStationID = context.stations.Single(x => x.name == startingStation).id;
            int endStationID = context.stations.Single(x => x.name == endStation).id;

            var lineStations = this.GetAllLineStations();

            foreach (var line in (lineStations.Where(x => x.station_id == startingStationID).Select(x => (int)x.line_id)).ToArray())//select lines that have starting station 
            {
                var route = this.GetSchedule(line, startingStationID, endStationID, selectedTime);

                var routeInfo = new Route();
                routeInfo.Departure = route.First().Arrival;
                routeInfo.LineChanges = 1;
                routeInfo.Cost = 1.60;
                routeInfo.stationSchedule = route.ToList();

                if (route.Last().StationId == endStationID)
                {
                    routeInfo.Arrival = route.Last().Arrival;
                    routeInfo.Duration = routeInfo.Arrival - routeInfo.Departure;
                    directRoutes.Add(routeInfo);
                }
                else indirectRoutes.Add(line, routeInfo);
            }

            foreach (var lineID in indirectRoutes.Keys.ToArray())
            {
                int? intersection = indirectRoutes[lineID].stationSchedule.Last().StationId;
                var secondLineIDs = lineStations.Where(x => x.station_id == intersection || x.station_id == endStationID)
                .GroupBy(x => x.line_id)
                .Select(grp => new { Count = grp.Count(), LineID = grp.Key })
                .Where(g => g.Count == 2).Select(c => c.LineID).ToArray(); //select all lines containing intersection and endStation

                var oldRoute = new Route(indirectRoutes[lineID]);

                if (oldRoute.stationSchedule.Count > 1)
                {
                    foreach (var secondLineID in secondLineIDs)
                    {
                        var lastTime = indirectRoutes[lineID].stationSchedule.Last().Arrival;
                        var newRouteList = this.GetSchedule((int)secondLineID, (int)intersection, endStationID, lastTime);
                        var route = indirectRoutes[lineID].stationSchedule.Union(newRouteList).ToList();

                        var routeInfo = indirectRoutes[lineID];
                        routeInfo.stationSchedule = route;
                        routeInfo.LineChanges++;
                        routeInfo.Cost += 1.60;
                        routeInfo.Arrival = route.Last().Arrival;
                        routeInfo.Duration = routeInfo.Arrival - routeInfo.Departure;
                        directRoutes.Add(routeInfo);//add new route to direct routes for display
                        indirectRoutes[lineID] = new Route(oldRoute);
                    }
                }

            }
            switch (sortOption)
            {
                case "duration":
                    directRoutes = directRoutes.OrderBy(r => r.Duration).ToList();
                    break;
                case "cost":
                    directRoutes = directRoutes.OrderBy(r => r.Cost).ToList();
                    break;
                case "waiting":
                    directRoutes = directRoutes.OrderBy(r => r.Departure - DateTime.Now.TimeOfDay).ToList();
                    break;

            }
            return directRoutes;
        }

        private IEnumerable<StationSchedule> GetSchedule(int lineID, int firstStationID, int lastStationID, TimeSpan? currentTime)
        {
            var routeList = new List<StationSchedule>();

            if (firstStationID < lastStationID)
            {
                routeList = (from ls in context.line_station
                             where ls.line_id == lineID && ls.station_id >= firstStationID && ls.station_id <= lastStationID//select stations belonging to that line
                             join station in context.stations
                             on ls.station_id equals station.id
                             join line in context.lines
                             on ls.line_id equals line.ID
                             join transport in context.transport_types
                             on line.transporttype_id equals transport.id
                             select new StationSchedule()
                             {
                                 StationId = station.id,
                                 Name = station.name,
                                 LineName = String.Format("{0} ({1})", line.name, line.company),
                                 TranportType = transport.name
                             }).ToList();//select station name and id and add that to the routeList
            }
            else
            {
                routeList = (from ls in context.line_station
                             where ls.line_id == lineID && ls.station_id <= firstStationID && ls.station_id >= lastStationID//select stations belonging to that line
                             join station in context.stations
                             on ls.station_id equals station.id
                             join line in context.lines
                             on ls.line_id equals line.ID
                             join transport in context.transport_types
                             on line.transporttype_id equals transport.id
                             select new StationSchedule()
                             {
                                 StationId = station.id,
                                 Name = station.name,
                                 LineName = String.Format("{0} ({1})", line.name, line.company),//Add line and company
                                 TranportType = transport.name
                             }).ToList().OrderByDescending(x => x.StationId).ToList();
            }

            foreach (var station in routeList)
            {
                try
                {
                    if (firstStationID < lastStationID)
                    {
                        station.Arrival = context.schedules.ToList()//name to the first station name   
                        .First(x => x.direction == 0 && x.station_id == station.StationId && x.line_id == lineID && x.arrival > currentTime).arrival;//select first arrival time
                        currentTime = station.Arrival;//sets currentTime to this stations arrival to ensure that next stations arrival is greater than the previous one  
                    }
                    else
                    {
                        station.Arrival = context.schedules.ToList()
                       .First(x => x.direction == 1 && x.station_id == station.StationId && x.line_id == lineID && x.arrival > currentTime).arrival;
                        currentTime = station.Arrival;
                    }
                }
                catch { }
            }
            return routeList;
        }

        public IEnumerable<stations> GetAllStations()
        {
            return context.stations.OrderBy(s => s.name).ToList();
        }

        public IEnumerable<line_station> GetAllLineStations()
        {
            return context.line_station.ToList();
        }

        public void updateTable()//should be used within Startup.Configure() - runs only once
        {
            var stationIDs = context.schedules.Where(x => x.line_id == 4).Select(x => x.station_id).ToList();
            var y = stationIDs.Distinct().OrderByDescending(x => x).ToList();
            int i = 0;
            var id = 5579;
            foreach (var schedule in context.schedules.Where(x => x.line_id == 4).ToList())
            {
                context.schedules.Add(new schedules() { id = id, arrival = schedule.arrival, direction = 1, line_id = 4, station_id = y[i] });
                if (y[i] == y.Last()) i = -1;
                id++;
                i++;      
            }

            context.SaveChanges();
        }

        public bool SaveAll()
        {
            return context.SaveChanges() > 0;
        }

        public trips UpdateTrip(trips newTrip)
        {
            var trip = context.trips
                .Where(t => t.DriverId == newTrip.DriverId && t.VehicleId == newTrip.VehicleId && t.ScheduleId == newTrip.ScheduleId)
                .FirstOrDefault();
            if(trip != null)
            {
                trip.Latitude = newTrip.Latitude;
                trip.Longitude = newTrip.Longitude;
                newTrip = trip;
                return newTrip;
            }
            return null;
        }

        public IEnumerable<trips> GetAllTrips()
        {
            return context.trips;
        }

        public void AddTrip(trips newTrip)
        {
            context.trips.Add(newTrip);
        }
    }
}