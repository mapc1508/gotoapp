using System;
using System.Collections.Generic;

namespace GoTo.Models
{
    public partial class line_station
    {
        public int id { get; set; }
        public int? line_id { get; set; }
        public int? station_id { get; set; }
    }
}
