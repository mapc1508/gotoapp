﻿using GoTo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoTo.Models
{
    public class StationSchedule
    {
        public TimeSpan? Arrival { get; set; }
        public string LineName { get; set; }
        public string Name { get; set; }
        public int StationId { get; set; }
        public string TranportType { get; set; }
    }
}
