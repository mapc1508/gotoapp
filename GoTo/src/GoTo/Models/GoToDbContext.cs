using GoTo.Models;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Metadata;

namespace GoTo
{
    public partial class GoToDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(@"Server=tcp:mapc1508.database.windows.net,1433;Database=GoToDb;User ID=mapc1508@mapc1508;Password=Trzalica1806.;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<line_station>(entity =>
            {
                entity.Property(e => e.id).ValueGeneratedNever();
            });

            modelBuilder.Entity<lines>(entity =>
            {
                entity.Property(e => e.ID).ValueGeneratedNever();

                entity.Property(e => e.company)
                    .HasMaxLength(45)
                    .HasColumnType("varchar");

                entity.Property(e => e.created_at).HasColumnType("datetime");

                entity.Property(e => e.days)
                    .HasMaxLength(45)
                    .HasColumnType("varchar");

                entity.Property(e => e.end_station)
                    .HasMaxLength(45)
                    .HasColumnType("varchar");

                entity.Property(e => e.name)
                    .HasMaxLength(45)
                    .HasColumnType("varchar");

                entity.Property(e => e.start_station)
                    .HasMaxLength(45)
                    .HasColumnType("varchar");

                entity.Property(e => e.updated_at).HasColumnType("datetime");
            });

            modelBuilder.Entity<operators>(entity =>
            {
                entity.Property(e => e.id).ValueGeneratedNever();

                entity.Property(e => e.city)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnType("varchar");

                entity.Property(e => e.country)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnType("varchar");

                entity.Property(e => e.created_at).HasColumnType("datetime");

                entity.Property(e => e.description)
                    .HasMaxLength(45)
                    .HasColumnType("varchar");

                entity.Property(e => e.name)
                    .IsRequired()
                    .HasMaxLength(45)
                    .HasColumnType("varchar");

                entity.Property(e => e.updated_at).HasColumnType("datetime");
            });

            modelBuilder.Entity<schedules>(entity =>
            {
                entity.Property(e => e.id).ValueGeneratedNever();

                entity.Property(e => e.created_at).HasColumnType("datetime");

                entity.Property(e => e.name)
                    .HasMaxLength(45)
                    .HasColumnType("varchar");

                entity.Property(e => e.updated_at).HasColumnType("datetime");
            });

            modelBuilder.Entity<stations>(entity =>
            {
                entity.Property(e => e.id).ValueGeneratedNever();

                entity.Property(e => e.created_at).HasColumnType("datetime");

                entity.Property(e => e.name)
                    .HasMaxLength(45)
                    .HasColumnType("varchar");

                entity.Property(e => e.updated_at).HasColumnType("datetime");
            });

            modelBuilder.Entity<transport_types>(entity =>
            {
                entity.Property(e => e.id).ValueGeneratedNever();

                entity.Property(e => e.created_at).HasColumnType("datetime");

                entity.Property(e => e.name)
                    .HasMaxLength(45)
                    .HasColumnType("varchar");

                entity.Property(e => e.updated_at).HasColumnType("datetime");
            });
        }

        public virtual DbSet<line_station> line_station { get; set; }
        public virtual DbSet<lines> lines { get; set; }
        public virtual DbSet<operators> operators { get; set; }
        public virtual DbSet<schedules> schedules { get; set; }
        public virtual DbSet<stations> stations { get; set; }
        public virtual DbSet<transport_types> transport_types { get; set; }
        public virtual DbSet<vehicles> vehicles { get; set; }
        public virtual DbSet<drivers> drivers { get; set; }
        public virtual DbSet<trips> trips { get; set; }
    }
}