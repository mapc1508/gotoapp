using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoTo.Models
{
    public class trips
    {
        public int ID { get; set; }
        public int ScheduleId { get; set; }
        public int DriverId { get; set; }
        public int VehicleId { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public vehicles vechicle { get; set; }
        public drivers driver { get; set; }
        public schedules schedule { get; set; }
    }
}
