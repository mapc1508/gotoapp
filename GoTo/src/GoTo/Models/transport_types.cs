using System;
using System.Collections.Generic;

namespace GoTo.Models
{
    public partial class transport_types
    {
        public int id { get; set; }
        public DateTime? created_at { get; set; }
        public string name { get; set; }
        public DateTime? updated_at { get; set; }
    }
}
