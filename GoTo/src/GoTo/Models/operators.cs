using System;
using System.Collections.Generic;

namespace GoTo.Models
{
    public partial class operators
    {
        public int id { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public DateTime? created_at { get; set; }
        public string description { get; set; }
        public string name { get; set; }
        public DateTime? updated_at { get; set; }
    }
}
