﻿using System;
using System.Collections.Generic;

namespace GoTo.Models
{
    public class Route
    {
        public TimeSpan? Duration { get; set; }
        public TimeSpan? Departure { get; set; }
        public TimeSpan? Arrival { get; set; }
        public double Cost { get; set; }
        public int LineChanges { get; set; }
        public List<StationSchedule> stationSchedule { get; set; }

        public Route()
        {

        }

        public Route(Route other)//copy constructor
        {
            this.Arrival = other.Arrival;
            this.Cost = other.Cost;
            this.Departure = other.Departure;
            this.Duration = other.Duration;
            this.LineChanges = other.LineChanges;
            this.stationSchedule = other.stationSchedule;
        }
    }
}