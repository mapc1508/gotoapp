using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoTo.Models
{
    public class vehicles
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<trips> Trips { get; set; }
    }
}
