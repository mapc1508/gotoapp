﻿using System;
using System.Collections.Generic;

namespace GoTo.Models
{
    public interface IGoToRepository
    {
        IEnumerable<line_station> GetAllLineStations();
        IEnumerable<stations> GetAllStations();
        List<Route> GetRoute(string startingStation, string endStation, TimeSpan? selectedTime, string sortOption = "duration");
        void updateTable();
        bool SaveAll();
        trips UpdateTrip(trips result);
        IEnumerable<trips> GetAllTrips();
        void AddTrip(trips newTrip);
    }
}