﻿"use strict";
//MODULE
var goToApp = angular.module("goToApp", ['ngRoute']);

//ROUTING
goToApp.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: "/home.htm",
            controller: "homeController"
        })
        .when('/routes', {
        templateUrl: "/routes.htm",
        controller: "routeController"
        })
    .otherwise({
        template: "This route isn't set"
    })
});

//SERVICES
goToApp.service("routeService", function () {
    this.startStation = "";
    this.endStation = "";
    this.selectedTime = "";
    this.sortOption = "";
});

//CONTROLLERS
goToApp.controller("homeController", ['$scope', '$http', '$filter', '$interval', '$location', 'routeService',
    function ($scope, $http, $filter, $interval, $location, $routeService) {

    $scope.stations = [];
    $http.get("api/stations")      
        .then(
         //Success callback
        function (response) {
            angular.copy(response.data, $scope.stations);
        },
        //Failure callback
        function () {
            console.log("Something is wrong with the API call");
        });

    $scope.sortOption = "";
    $scope.timeValue = $filter('date')(new Date(), "HH:mm:ss");
    $scope.updateTime = function () {
        $scope.timeValue = $filter('date')(new Date(), "HH:mm:ss");
    };
    $interval($scope.updateTime, 1000);

    $scope.startStation = "";
    $scope.endStation = "";
    $scope.currentTime = new Date();
    $scope.selectedTime = new Date(1970, 0, 1, $scope.currentTime.getHours(), $scope.currentTime.getMinutes(), $scope.currentTime.getSeconds());

    $scope.routes = [];

    $scope.$watchCollection('[startStation, endStation, selectedTime, sortOption]', function (newValues) {
        $routeService.startStation = newValues[0];
        $routeService.endStation = newValues[1];
        $routeService.selectedTime = newValues[2];
        $routeService.sortOption = newValues[3];
    });

    $scope.submitSearch = function () {
       $location.path("/routes");
    } 
}]);

goToApp.controller("routeController", ['$scope', 'routeService', '$http','$timeout', function ($scope, $routeService, $http, $timeout) {
    $scope.routes = [];
    $scope.isSearching = true;
    $scope.departure = $routeService.startStation;
    $scope.arrival = $routeService.endStation;

    $http.get("/api/route/" + $routeService.startStation + "/" + $routeService.endStation + "/"
        + $routeService.selectedTime.toLocaleTimeString() + "/" + $routeService.sortOption)
     .then(
         function (response) {
             angular.copy(response.data, $scope.routes);
         },
         function () {
             console.log("Something is wrong with the API call");
         })
        .finally(function () {
            $scope.isSearching = false;
            var lineNumber = 0;
            $scope.treeObjects = [];
            var stationNumber = 0;
            var cost = 0;
            $scope.searchResults = [];

            for (var i = 0; i < $scope.routes.length; i++) {
                stationNumber = 1;
                lineNumber = 0;
                var stationSchedule = $scope.routes[i].stationSchedule;

                $scope.treeObjects.push({
                    text: "<div class='routeInfo'><b>Dep: </b>" + $scope.routes[i].departure.substr(0,5) + "</div>" +
                         "<div class='routeInfo'><b>Arr: </b>" + $scope.routes[i].arrival.substr(0, 5) + "</div>" +
                         "<div class='routeInfo'><b>Cost: </b> " + $scope.routes[i].cost + "</div>" +
                         "<div class='routeInfo'><b>Dur: </b>" + $scope.routes[i].duration.substr(0, 5) + "</div>" +
                         //"<div class='routeInfo'><b>Chg: </b> " + $scope.routes[i].lineChanges + "</div>" +
                         "<div class='badge treeViewBadge'><i class='fa fa-exchange'></i><span> " + $scope.routes[i].lineChanges + "</span></div>",
                    state: {
                        expanded: false,
                    },
                    backColor: "#EDEDED",
                    nodes:[]
                });

                for (var y = 0; y < stationSchedule.length; y++) {  
                    var transportIcon = "";
                    switch (stationSchedule[y].tranportType) {
                        case "tram":
                            transportIcon = "<div class='treeViewBadge'><i class='material-icons'>&#xE571;</i></div>";
                            break;
                        case "bus":
                            transportIcon = "<div class='treeViewBadge'><i class='material-icons'>&#xE530;</i></div>";
                            break;
                    }
                    if (y == 0 || (stationSchedule[y].lineName != stationSchedule[y - 1].lineName)) {
                        if (stationNumber == 1) {
                            $scope.treeObjects[i].nodes.push({
                                text: "<b>Line:</b> " + stationSchedule[y].lineName + transportIcon,
                                nodes: []
                            });
                        }
                        else {
                            lineNumber++;
                            $scope.treeObjects[i].nodes.push({
                                text: "<b>Line:</b> " + stationSchedule[y].lineName +
                                " - <span style='color:#db1111'><b>Change lines!</b></span>" + transportIcon,
                                nodes: []
                            });
                        }
                        stationNumber = 1;
                    }
                    $scope.treeObjects[i].nodes[lineNumber].nodes[stationNumber - 1] = {
                        text: "<div class='treeViewStation'>" + stationNumber + ") " + stationSchedule[y].arrival + " - " + stationSchedule[y].name + "</div>"
                    };
                    stationNumber++;
                }
                function getTree() {
                    var tree = [$scope.treeObjects[i]];
                    return tree;
                };
                $scope.treeName = '#tree' + i;
                $($scope.treeName).treeview({ data: getTree(), borderColor: '#ABABAB', showTags: true });
            };
            //console.log(treeObjects);
            //function getTree() {
            //    var tree = [treeObjects[0]];
            //    return tree;
            //};
            //$('#tree').treeview({ data: getTree(), borderColor: '#ABABAB', showTags: true });
        });
}]);