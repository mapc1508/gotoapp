﻿using AutoMapper;
using GoTo.Models;
using GoTo.ViewModels;
using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace GoTo.Controllers.Api
{
    [Route("api/")]
    public class GoToController: Controller
    {
        private IGoToRepository repository;

        public GoToController(IGoToRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet("stations")]
        public JsonResult Get()
        {
            var allStations = repository.GetAllStations();
            return Json(allStations);
        }

        [HttpGet("route/{startStation}/{endStation}/{time}/{sortOption?}")]
        public JsonResult Get(string startStation, string endStation, string time, string sortOption = "duration")
        {
            var selectedTime = Convert.ToDateTime(time).TimeOfDay;
            var result = repository.GetRoute(startStation, endStation, selectedTime, sortOption);
            return Json(result);
        }

        [HttpPost("updatePosition")]
        public JsonResult Post([FromBody]tripsViewModel tripsViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var newTrip = Mapper.Map<trips>(tripsViewModel);
                    newTrip = repository.UpdateTrip(newTrip);
                    if (newTrip != null)//if such trip exists
                    {
                        if (repository.SaveAll())//if there's any changes made
                        {
                            Response.StatusCode = (int)HttpStatusCode.Created;
                            return Json(Mapper.Map<tripsViewModel>(newTrip));
                        }
                    }     
                }
            }
            catch (Exception)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Something went wrong. Trip has not been updated.");
            }
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json("Something went wrong. Trip has not been updated." );
        }

        [HttpGet("trips")]
        public JsonResult GetTrips()
        {
            try
            {               
                var result = Mapper.Map<IEnumerable<tripsViewModel>>(repository.GetAllTrips());
                return Json(result);
            }
            catch (Exception)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Failed to return trips");
            }
        }

        [HttpPost("addTrip")]
        public JsonResult AddTrip([FromBody]tripsViewModel tripsViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var newTrip = Mapper.Map<trips>(tripsViewModel);
                    repository.AddTrip(newTrip);
                        if (repository.SaveAll())
                        {
                            Response.StatusCode = (int)HttpStatusCode.Created;
                            return Json(Mapper.Map<tripsViewModel>(newTrip));
                        }
                }
            }
            catch (Exception)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Something went wrong. Trip has not been added.");
            }
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Json("Something went wrong. Trip has not been added.");
        }
    }
}