﻿using Microsoft.AspNet.Mvc;

namespace GoTo.Controllers.Web
{
    public class AppController: Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}