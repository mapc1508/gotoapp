﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GoTo.ViewModels
{
    public class tripsViewModel
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "ScheduleId is required")]
        public int? ScheduleId { get; set; }
        [Required(ErrorMessage = "DriverId is required")]
        public int? DriverId { get; set; }
        [Required(ErrorMessage = "VehicleId is required")]
        public int? VehicleId { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
