using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace GoTo.Migrations
{
    public partial class TripsLngLatNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_trips_drivers_DriverId", table: "trips");
            migrationBuilder.DropForeignKey(name: "FK_trips_vehicles_VehicleId", table: "trips");
            migrationBuilder.AlterColumn<double>(
                name: "Longitude",
                table: "trips",
                nullable: true);
            migrationBuilder.AlterColumn<double>(
                name: "Latitude",
                table: "trips",
                nullable: true);
            migrationBuilder.AddForeignKey(
                name: "FK_trips_drivers_DriverId",
                table: "trips",
                column: "DriverId",
                principalTable: "drivers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_trips_vehicles_VehicleId",
                table: "trips",
                column: "VehicleId",
                principalTable: "vehicles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_trips_drivers_DriverId", table: "trips");
            migrationBuilder.DropForeignKey(name: "FK_trips_vehicles_VehicleId", table: "trips");
            migrationBuilder.AlterColumn<double>(
                name: "Longitude",
                table: "trips",
                nullable: false);
            migrationBuilder.AlterColumn<double>(
                name: "Latitude",
                table: "trips",
                nullable: false);
            migrationBuilder.AddForeignKey(
                name: "FK_trips_drivers_DriverId",
                table: "trips",
                column: "DriverId",
                principalTable: "drivers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_trips_vehicles_VehicleId",
                table: "trips",
                column: "VehicleId",
                principalTable: "vehicles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
