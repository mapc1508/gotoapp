using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using GoTo;

namespace GoTo.Migrations
{
    [DbContext(typeof(GoToDbContext))]
    partial class GoToDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("GoTo.Models.drivers", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("GoTo.Models.line_station", b =>
                {
                    b.Property<int>("id");

                    b.Property<int?>("line_id");

                    b.Property<int?>("station_id");

                    b.HasKey("id");
                });

            modelBuilder.Entity("GoTo.Models.lines", b =>
                {
                    b.Property<int>("ID");

                    b.Property<string>("company")
                        .HasAnnotation("MaxLength", 45)
                        .HasAnnotation("Relational:ColumnType", "varchar");

                    b.Property<DateTime?>("created_at")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<string>("days")
                        .HasAnnotation("MaxLength", 45)
                        .HasAnnotation("Relational:ColumnType", "varchar");

                    b.Property<string>("end_station")
                        .HasAnnotation("MaxLength", 45)
                        .HasAnnotation("Relational:ColumnType", "varchar");

                    b.Property<string>("name")
                        .HasAnnotation("MaxLength", 45)
                        .HasAnnotation("Relational:ColumnType", "varchar");

                    b.Property<string>("start_station")
                        .HasAnnotation("MaxLength", 45)
                        .HasAnnotation("Relational:ColumnType", "varchar");

                    b.Property<int?>("transporttype_id");

                    b.Property<DateTime?>("updated_at")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.HasKey("ID");
                });

            modelBuilder.Entity("GoTo.Models.operators", b =>
                {
                    b.Property<int>("id");

                    b.Property<string>("city")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 45)
                        .HasAnnotation("Relational:ColumnType", "varchar");

                    b.Property<string>("country")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 45)
                        .HasAnnotation("Relational:ColumnType", "varchar");

                    b.Property<DateTime?>("created_at")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<string>("description")
                        .HasAnnotation("MaxLength", 45)
                        .HasAnnotation("Relational:ColumnType", "varchar");

                    b.Property<string>("name")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 45)
                        .HasAnnotation("Relational:ColumnType", "varchar");

                    b.Property<DateTime?>("updated_at")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.HasKey("id");
                });

            modelBuilder.Entity("GoTo.Models.schedules", b =>
                {
                    b.Property<int>("id");

                    b.Property<TimeSpan?>("arrival");

                    b.Property<DateTime?>("created_at")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<int>("direction");

                    b.Property<int?>("line_id");

                    b.Property<string>("name")
                        .HasAnnotation("MaxLength", 45)
                        .HasAnnotation("Relational:ColumnType", "varchar");

                    b.Property<int?>("station_id");

                    b.Property<DateTime?>("updated_at")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.HasKey("id");
                });

            modelBuilder.Entity("GoTo.Models.stations", b =>
                {
                    b.Property<int>("id");

                    b.Property<DateTime?>("created_at")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<double?>("latitude");

                    b.Property<double?>("longitude");

                    b.Property<string>("name")
                        .HasAnnotation("MaxLength", 45)
                        .HasAnnotation("Relational:ColumnType", "varchar");

                    b.Property<DateTime?>("updated_at")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.HasKey("id");
                });

            modelBuilder.Entity("GoTo.Models.transport_types", b =>
                {
                    b.Property<int>("id");

                    b.Property<DateTime?>("created_at")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.Property<string>("name")
                        .HasAnnotation("MaxLength", 45)
                        .HasAnnotation("Relational:ColumnType", "varchar");

                    b.Property<DateTime?>("updated_at")
                        .HasAnnotation("Relational:ColumnType", "datetime");

                    b.HasKey("id");
                });

            modelBuilder.Entity("GoTo.Models.trips", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("DriverId");

                    b.Property<double?>("Latitude");

                    b.Property<double?>("Longitude");

                    b.Property<int>("ScheduleId");

                    b.Property<int>("VehicleId");

                    b.Property<int?>("vechicleId");

                    b.HasKey("ID");
                });

            modelBuilder.Entity("GoTo.Models.vehicles", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("GoTo.Models.trips", b =>
                {
                    b.HasOne("GoTo.Models.drivers")
                        .WithMany()
                        .HasForeignKey("DriverId");

                    b.HasOne("GoTo.Models.schedules")
                        .WithMany()
                        .HasForeignKey("ScheduleId");

                    b.HasOne("GoTo.Models.vehicles")
                        .WithMany()
                        .HasForeignKey("vechicleId");
                });
        }
    }
}
