using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace GoTo.Migrations
{
    public partial class AddSchedulesDirection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "direction",
                table: "schedules",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(name: "direction", table: "schedules");
        }
    }
}
