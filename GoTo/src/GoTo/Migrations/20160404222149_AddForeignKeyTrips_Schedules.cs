using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace GoTo.Migrations
{
    public partial class AddForeignKeyTrips_Schedules : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_trips_drivers_DriverId", table: "trips");
            migrationBuilder.DropForeignKey(name: "FK_trips_vehicles_VehicleId", table: "trips");
            migrationBuilder.AddColumn<int>(
                name: "vechicleId",
                table: "trips",
                nullable: true);
            migrationBuilder.AddForeignKey(
                name: "FK_trips_drivers_DriverId",
                table: "trips",
                column: "DriverId",
                principalTable: "drivers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_trips_schedules_ScheduleId",
                table: "trips",
                column: "ScheduleId",
                principalTable: "schedules",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_trips_vehicles_vechicleId",
                table: "trips",
                column: "vechicleId",
                principalTable: "vehicles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_trips_drivers_DriverId", table: "trips");
            migrationBuilder.DropForeignKey(name: "FK_trips_schedules_ScheduleId", table: "trips");
            migrationBuilder.DropForeignKey(name: "FK_trips_vehicles_vechicleId", table: "trips");
            migrationBuilder.DropColumn(name: "vechicleId", table: "trips");
            migrationBuilder.AddForeignKey(
                name: "FK_trips_drivers_DriverId",
                table: "trips",
                column: "DriverId",
                principalTable: "drivers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_trips_vehicles_VehicleId",
                table: "trips",
                column: "VehicleId",
                principalTable: "vehicles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
