using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace GoTo.Migrations
{
    public partial class InitialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "line_station",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    line_id = table.Column<int>(nullable: true),
                    station_id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_line_station", x => x.id);
                });
            migrationBuilder.CreateTable(
                name: "lines",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    company = table.Column<string>(type: "varchar(45)", nullable: true),
                    created_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    days = table.Column<string>(type: "varchar(45)", nullable: true),
                    end_station = table.Column<string>(type: "varchar(45)", nullable: true),
                    name = table.Column<string>(type: "varchar(45)", nullable: true),
                    start_station = table.Column<string>(type: "varchar(45)", nullable: true),
                    transporttype_id = table.Column<int>(nullable: true),
                    updated_at = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_lines", x => x.ID);
                });
            migrationBuilder.CreateTable(
                name: "operators",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    city = table.Column<string>(type: "varchar(45)", nullable: false),
                    country = table.Column<string>(type: "varchar(45)", nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    description = table.Column<string>(type: "varchar(45)", nullable: true),
                    name = table.Column<string>(type: "varchar(45)", nullable: false),
                    updated_at = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_operators", x => x.id);
                });
            migrationBuilder.CreateTable(
                name: "schedules",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    arrival = table.Column<TimeSpan>(nullable: true),
                    created_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    line_id = table.Column<int>(nullable: true),
                    name = table.Column<string>(type: "varchar(45)", nullable: true),
                    station_id = table.Column<int>(nullable: true),
                    updated_at = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_schedules", x => x.id);
                });
            migrationBuilder.CreateTable(
                name: "stations",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    latitude = table.Column<double>(nullable: true),
                    longitude = table.Column<double>(nullable: true),
                    name = table.Column<string>(type: "varchar(45)", nullable: true),
                    updated_at = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_stations", x => x.id);
                });
            migrationBuilder.CreateTable(
                name: "transport_types",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false),
                    created_at = table.Column<DateTime>(type: "datetime", nullable: true),
                    name = table.Column<string>(type: "varchar(45)", nullable: true),
                    updated_at = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_transport_types", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable("line_station");
            migrationBuilder.DropTable("lines");
            migrationBuilder.DropTable("operators");
            migrationBuilder.DropTable("schedules");
            migrationBuilder.DropTable("stations");
            migrationBuilder.DropTable("transport_types");
        }
    }
}
